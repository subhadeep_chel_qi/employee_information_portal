Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get 'signup'  => 'users#new'
  root 'homes#index'
  get 'admin' => 'admin/dashboards#index'
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
  get 'holiday' => 'holidays#index'
  namespace :admin do
    resources :dashboards
    resources :holidays
    resources :users
    get 'applied_leaves' => 'dashboard#applied_leaves'
    resources :emails
  end

  resources :users ,only: [:show, :new, :create, :edit, :update, :index] do
    resources :leaveemps
    get 'upcoming_leaves' => 'leaveemps#upcoming_leaves'
    get 'leave_history' => 'leaveemps#leave_history'
  end

  resources :roles
  resources :admin

  get 'users/add_manager'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
end
