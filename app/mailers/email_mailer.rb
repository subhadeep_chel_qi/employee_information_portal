class EmailMailer < ApplicationMailer
  def send_email(email)
    @email = email
    mail to: email.receiver_email, subject: email.subject
  end
end
