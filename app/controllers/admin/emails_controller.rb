class Admin::EmailsController < ApplicationController
  before_action :logged_in_admin
  layout "admin"

  def index
    @emails = Email.order(:id).paginate(page: params[:page], :per_page => 10)
  end

  def new
    @email = Email.new
    @user_emails = User.order(:id)
  end

  def create
    @email = Email.new(email_params.merge(sender_email: current_user.email, status: :not_sent))

    if @email.save
      #EmailMailer.send_email(@email).deliver
      EmailWorker.perform_async()
      redirect_to admin_emails_path, notice: 'Email Successfully Created'
    else
      render 'new'
    end
  end

  def destroy
    @email = Email.find(params[:id])
    @email.destroy

    redirect_to admin_emails_path, notice: 'Email Deleted Successfully'
  end

  def logged_in_admin
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end

  private
    def email_params
      params.require(:email).permit(:receiver_email, :subject, :body)
    end
end