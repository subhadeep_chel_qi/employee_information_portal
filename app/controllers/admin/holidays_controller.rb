class Admin::HolidaysController < ApplicationController
  layout "admin"
  def index
    @holidays = Holiday.all
  end

  def show
    @holidays = Holiday.all
  end

  def new
    @holiday= Holiday.new
  end

  def edit
    @holiday = Holiday.find(params[:id])
  end

  def update
    @holiday = Holiday.find(params[:id])
    if @holiday.update(holiday_params)
      flash[:notice] = 'Updated Sucessfully'
      redirect_to admin_holidays_path
    else
      flash[:notice] = 'Do Not Updated Sucessfully'
      redirect_to admin_holidays_path
    end
  end

  def create
    @holiday = Holiday.new(holiday_params)
    if Holiday.where(date: @holiday.date).exists? || @holiday.date < Date.today
      redirect_to admin_holidays_path
      flash[:notice] = 'Holiday Already Exsists or The date is already expired'
    else
      if @holiday.save
        redirect_to admin_holidays_path
      else
        render 'new'
      end
    end
  end

  def destroy
    Holiday.find(params[:id]).destroy
    flash[:success] = 'Holiday Successfully deleted'
    redirect_to admin_holidays_path
  end

  def holiday_params
    params.require(:holiday).permit(:date, :reason)
  end

  def check_log_in
    if current_user.nil?
      redirect_to login_path
    end
  end
end