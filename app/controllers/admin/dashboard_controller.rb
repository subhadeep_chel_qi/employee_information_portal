class Admin::DashboardController < ApplicationController
  layout "admin"
  before_action :logged_in_admin
  def index

  end

  def applied_leaves
    @leaves = Leaveemp.where("startdate > ?", Time.zone.now.to_date).order(:startdate)
  end

  def logged_in_admin
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end
end
