class Admin::DashboardsController < ApplicationController
  before_action :check_log_in
  layout "admin"
  #before_action :authorize_admins , only: [:index]
  def index
    @user = User.find(current_user.id)
    @name = @user.name
  end
  def check_log_in
    if current_user.nil?
      redirect_to login_path
    end
  end
  def authorize_admins
    unless current_user.admin?
      redirect_to root_path
    end
  end
end
