class LeaveempsController < ApplicationController
  before_action :logged_in_user
  before_action :find_user_leave, only: [:edit, :update, :destroy]
  before_action :check_designation

  def index
    @leaves = Leaveemp.where(user_id: current_user.id).order(:startdate)
  end

  def new
    @leave  = current_user.leaveemps.build
  end

  def create
    @leave = current_user.leaveemps.build(leave_params)
    if @leave.save
      flash[:success] = 'Successfully applied for leave'
      redirect_to user_leaveemps_path
   else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @leave.update_attributes(leave_params)
      flash[:success] = 'Leave Application Updated'

      redirect_to user_leaveemps_path

    else
      render 'edit'
    end
  end

  def destroy
    @leave.destroy
    flash[:success] = 'Leave Successfully deleted'
    redirect_to user_leaveemps_path
  end

  def upcoming_leaves
    @leaves = Leaveemp.where("startdate > ? AND user_id = ?", Time.zone.now.to_date, current_user).order(:startdate)
  end

  def leave_history
    @leaves = Leaveemp.where("startdate < ? AND user_id = ?", Time.zone.now.to_date, current_user).order(:startdate)
  end

private
    def find_user_leave
      @leave = Leaveemp.find(params[:id])
    end

    def leave_params
      params.require(:leaveemp).permit(:startdate, :enddate, :comment)
    end

    def check_designation
      @user = User.find(params[:user_id])
      if UserRole.find_by(user_id: @user.id)
        @user_role_id = UserRole.find_by(user_id: params[:user_id]).role_id
        @designation = Role.find(@user_role_id).role
      else
        @designation = 'Not Assigned'
      end
    end
end
