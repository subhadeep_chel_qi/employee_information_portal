class ApplicationController < ActionController::Base
  include SessionsHelper
  include UsersHelper
  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end

  def fetchDesignation
    @user = User.find(params[:id])
    if UserRole.find_by(user_id: @user.id)
      @user_role_id = UserRole.find_by(user_id: params[:id]).role_id
      @designation = Role.find(@user_role_id).role
    else
      @designation = 'Not Assigned'
    end
  end
  private

    def logged_in_user
      unless logged_in?
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
end
