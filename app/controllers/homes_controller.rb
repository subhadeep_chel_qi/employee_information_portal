class HomesController < ApplicationController
  before_action :check_designation, except: [:index]
  def index
  end

  private
    def check_designation
      @designation = fetchDesignation
    end
end
