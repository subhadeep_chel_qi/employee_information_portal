class Email < ApplicationRecord
  validates :sender_email, presence: true
  validates :receiver_email, presence: true
  validates :subject, presence: true
  validates :body, presence: true

  enum status: { not_sent: 0, sent: 1 }
end
