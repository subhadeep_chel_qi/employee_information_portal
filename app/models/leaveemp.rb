class Leaveemp < ApplicationRecord
  belongs_to :user
  validates :user_id, presence: true
  validates :startdate, presence: true
  validates :enddate, presence: true
  validate :validate_leave_schedule

  private
    # validates startdate should always lesser than enddate
    def validate_leave_schedule
      if startdate.present? & enddate.present?
        if self.enddate < self.startdate
          errors.add(:Leave, " start date and leave end date should be valid")
        end
      end
    end
end
