class CreateEmployees < ActiveRecord::Migration[6.0]
  def change
    create_table :employees do |t|
      t.integer :emp_id
      t.integer :mng_id

      t.timestamps
    end
  end
end
