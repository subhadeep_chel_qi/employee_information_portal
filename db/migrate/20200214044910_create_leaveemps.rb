class CreateLeaveemps < ActiveRecord::Migration[6.0]
  def change
    create_table :leaveemps do |t|
      t.date :startdate
      t.date :enddate
      t.string :comment
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
