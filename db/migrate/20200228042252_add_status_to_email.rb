class AddStatusToEmail < ActiveRecord::Migration[6.0]
  def change
    add_column :emails, :status, :integer
  end
end
