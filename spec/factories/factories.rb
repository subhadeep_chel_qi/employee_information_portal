require 'factory_bot'

FactoryBot.define do
  factory :user do
    name {"Joe"}
    email {"joe@gmail.com"}
    mobile {'9999999999'}
    password {'123456789'}
    password_confirmation {'123456789'}
  end
  factory :user_role do
    user_id {6}
    role_id {10}
  end
end

FactoryBot.define do
  factory :manages do
    user_id {0}
  end
end

FactoryBot.define do
  factory :leaveemp do
    startdate {"2020-02-29"}
    enddate {"2020-02-29"}
    comment {6}
  end
end
