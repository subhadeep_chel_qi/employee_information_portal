require 'rails_helper'

RSpec.describe User, type: :model do
  context 'validation test' do
    it 'ensures name is present' do
      user = User.new(email: 'test@test.com',mobile: '9999999999',password: '123456789' ,password_confirmation: '123456789')
      expect(user).to_not be_valid
    end

    it 'ensures email is present' do
      user = User.new(name: 'Test User',email: 'test@test.com',mobile: '9999999999',password: '123456789' ,password_confirmation: '123456789')
      expect(user).to be_valid
    end

    it 'should save sucessfully' do
      user = User.new(name: 'Test User',email: 'test@test.com',mobile: '9999999999',password: '123456789' ,password_confirmation: '123456789').save
      expect(user).to eq(true)
    end

    context 'scope test' do
      let (:params) { {name: 'Test User',email: 'test@test.com',mobile: '9999999999',password: '123456789' ,password_confirmation: '123456789'} }
      before(:each) do
        User.new(params).save
        User.new(params).save
        #User.new(params.merge(active: true)).save
        #User.new(params.merge(active: false)).save
      end

      # it "should return active users" do
      #   expect(User.active_users.size).to eq(1)
      # end
    end
  end

  # context 'scope test' do
  # end


  #   let(:user) { User.new(name: 'Test User', email: 'test@test.com',mobile: '9999999999',password: '123456789' ,password_confirmation: '123456789') }

  # context 'validation' do
  #   context 'presence of email' do
  #     context 'if user does not have email,name,mobile,password ' do
  #       it 'should not be valid user' do
  #         user = User.new
  #         expect(user).to_not be_valid
  #       end
  #     end
  #   end
  # end

  # context 'minimum length of password' do
  #   it "password should have a minimum length" do
  #     user.password = user.password_confirmation = "a" * 7
  #     expect(user).to_not be_valid
  #   end
  # end

  # context 'minimum length of mobile number' do
  #   it "mobile number should have a minimum length of 10" do
  #     user.mobile = "9" * 7
  #     expect(user).to_not be_valid
  #   end
  # end

  # # context 'length of mobile number' do
  # #   it "mobile number should have a minimum length of 10" do
  # #     user.mobile = "9" * 10
  # #     expect(user).to_not be_valid
  # #   end
  # # end

  # context 'maximum length of phn number' do
  #   it "mobile number's length should not be grater than 10" do
  #     user.mobile = "9" * 12
  #     expect(user).to_not be_valid
  #   end
  # end

  # it "email should not be too long" do
  #     user.email = "a" * 266 + "@example.com"
  #     expect(user).to_not be_valid
  # end

  # it "name should not be too long" do
  #     user.email = "a" * 512
  #     expect(user).to_not be_valid
  # end

  #  it  "email addresses should be unique" do
  #     duplicate_user = user.dup
  #     user.save
  #     expect(duplicate_user).to_not be_valid
  #   end

end
