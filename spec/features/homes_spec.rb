require 'rails_helper'

RSpec.feature "Homes", type: :feature do
 before :each do
    user = User.create(name: 'Test User', email: 'test@test.com',mobile: '9999999999',password: '123456789' ,password_confirmation: '123456789')
    roles = Role.create([{ role: "Employee" }, { role: "Admin" }, { role: "Trainee"}, { role: "Intern"}])
    user.role = Role.find_by(role: 'Employee')
    holiday = Holiday.new
    user_role = UserRole.new
  end
  context 'home page ' do
   scenario "should have features" do
     visit "/"
     expect(page).to have_content('Employee Management Portal')
     expect(page).to have_link ("Home")
     expect(page).to have_link ("Login")
   end
  end
  context 'home page ' do
   scenario "should have features" do
    visit "/admin"
    fill_in 'Email', with: 'test@test.com'
    fill_in 'Password', with: '123456789'
    click_button("Log In")
     visit "/"
     expect(page).to have_link ("Home")
     expect(page).to have_link ("Logout")
    end
  end
end