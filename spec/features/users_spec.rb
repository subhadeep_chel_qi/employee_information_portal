require 'rails_helper'
#require 'capybara/rails'

RSpec.feature "Users", type: :feature do
  before :each do
    user = User.create(name: 'Test User', email: 'test@test.com',mobile: '9999999999',password: '123456789' ,password_confirmation: '123456789')
    roles = Role.create([{ role: "Employee" }, { role: "Admin" }, { role: "Trainee"}, { role: "Intern"}])
    user.role = Role.find_by(role: 'Employee')
    holiday = Holiday.new
    user_role = UserRole.new
  end
 context 'create new user' do
   scenario "should be signed in" do
      visit "/users/new"
        user = User.new
        fill_in 'user[name]', with: 'Test User'
        fill_in 'user[email]', with: 'test@test.com'
        fill_in 'user[mobile]', with: '9999999999'
        fill_in 'user[password]', with: '1234567890'
        fill_in 'user[password_confirmation]', with: '1234567890'
        click_button("Create My Account")
        expect(page).to have_current_path('/users')

    end
  end
end
